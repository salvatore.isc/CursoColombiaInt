//
//  ViewControllerXibEx.swift
//  CursoColombiaInt
//
//  Created by Salvador Lopez on 06/06/23.
//

import UIKit

class ViewControllerXibEx: UIViewController {

    @IBOutlet weak var containerShareBtn: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let sharedBtn = Bundle.main.loadNibNamed("ShareBtn", owner: self)?.first as? ShareBtn
        sharedBtn?.frame = containerShareBtn.bounds
        sharedBtn?.titleLb.setTitle("Titulo 1", for: .normal)
        containerShareBtn.addSubview(sharedBtn!)
        
        let shareBtn2 = Bundle.main.loadNibNamed("ShareBtn", owner: self)?.first as? ShareBtn
        shareBtn2?.frame = containerShareBtn.bounds
        shareBtn2?.titleLb.setTitle("Titulo 2", for: .normal)
        shareBtn2?.titleLb.addTarget(self, action: #selector(metodoPublic), for: .touchUpInside)
        self.view.addSubview(shareBtn2!)
    }
    
    @objc func metodoPublic(){
        print("hola desde el controller")
    }

}
