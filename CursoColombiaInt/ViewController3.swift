//
//  ViewController3.swift
//  CursoColombiaInt
//
//  Created by Salvador Lopez on 05/06/23.
//

import UIKit

protocol DataDelegateBack: AnyObject{
    func didReceiveData(_ data: Persona)
}

class ViewController3: UIViewController {
    
    //var myNumero: Int?
    //var myString: String?
    //var myBool: Bool?
    //var myPersona: Persona?
    
    var delegate: DataDelegateBack?
    var personaRegistro = Persona()
    
    @IBOutlet weak var numberTextField: UITextField! //Nombre
    @IBOutlet weak var stringTextField: UITextField! //Edad
    @IBOutlet weak var boolTextField: UITextField!
    @IBOutlet weak var personaTextField: UITextField!
    
    @IBAction func guardarAction(_ sender: Any) {
        //Colocar los datos a enviar (Nombre Edad)
        personaRegistro.nombre = numberTextField.text
        personaRegistro.edad = Int(stringTextField.text!)
        //Call delegate method
        //dump(personaRegistro)
        delegate?.didReceiveData(personaRegistro)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //if let myNumber2 = myNumero {
            //self.numberTextField.text = "\(myNumber2)"
        //}
        
        //if let myString2 = myString {
            //self.stringTextField.text = myString2
        //}
        
        //if let myBool2 = myBool {
            //self.boolTextField.text = myBool2 ? "Verdadero" : "Falso"
        //}
        
        //if let myPersona2 = myPersona {
            //if let name = myPersona2.nombre, let edad = myPersona2.edad {
                //self.personaTextField.text = "\(name), \(edad) (años)"
            //}
            
        //}
        
    }

}
