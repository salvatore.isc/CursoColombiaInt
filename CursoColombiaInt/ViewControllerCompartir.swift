//
//  ViewControllerCompartir.swift
//  CursoColombiaInt
//
//  Created by Salvador Lopez on 06/06/23.
//

import UIKit

class ViewControllerCompartir: UIViewController {

    @IBOutlet weak var txtShare: UILabel!
    
    let img = UIImage(named: "logo")
    
    @IBAction func shareAction(_ sender: Any) {
        let activityController = UIActivityViewController(activityItems: [self], applicationActivities: nil)
        present(activityController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

}

extension ViewControllerCompartir: UIActivityItemSource{
    
    //Determinar el tipo de datos
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return "Este es el placeholder del contenido"
    }
    
    //Colocar un tipo de dato (compartir), dependiendo de una actividad
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        // En caso de utilizar un switch colocar en el case: .message.self
        var valueToShare: Any!
        if activityType == .message {
            valueToShare = "Solo me mostrare en la app de #MESSAGE"
        }else if activityType == .mail{
            valueToShare = "Solo me muestro en la app de #MAIL"
        }else if activityType == .copyToPasteboard{
            valueToShare = "Me muestro en el portapapeles"
        }else{
            valueToShare = "Me muestro en casi todas las apps (ActivityType)"
        }
        return valueToShare
    }
    
    //Subject Field
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return "Aqui va el asunto..."
    }
}
