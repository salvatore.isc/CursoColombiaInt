//
//  ViewController.swift
//  CursoColombiaInt
//
//  Created by Salvador Lopez on 05/06/23.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    
    var sourceColor = UIColor.green
    //var currentPersona: Persona?
    var personas = [Persona]()

    @IBAction func testingPerformSegue(_ sender: Any) {
        performSegue(withIdentifier: "segueLogin", sender: (Any).self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("1. ViewDidLoad [VC]")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("2. ViewWillAppear [VC]")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("3. ViewDidAppear [VC]")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("4. ViewWillDissapear [VC]")
    }

    override func viewDidDisappear(_ animated: Bool) {
        print("5. ViewDidDissapear [VC]")
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let vcd = segue.destination as? ViewControllerDestino{
            vcd.color = sourceColor
        }
        
        if let vcRegistro = segue.destination as? ViewController3{
            vcRegistro.delegate = self
        }
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        var performSegue = false
        
        if identifier == "segueLogin"{
            //Evaluar la entrada del usuario [USER, PASS]
            //if usernameTextField.text == "name" && passTextField.text == "pass"{
                //performSegue = true
            //}
            self.personas.forEach { p in
                if usernameTextField.text == p.nombre && passTextField.text == "pass"{
                    performSegue = true
                }
            }
        }else if identifier == "segueRegistro"{
            performSegue = true
        }else if identifier == "otro"{
            performSegue = true
        }
        
        return performSegue
    }
    
    @IBAction func selectingValue(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.sourceColor = UIColor.green
        }else if sender.selectedSegmentIndex == 1 {
            self.sourceColor = UIColor.blue
        }
    }
    
    @IBAction func presentManually(_ sender: Any) {
        //let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //if let viewControllerDestino = storyboard.instantiateViewController(withIdentifier: "VC3") as? ViewController3 {
            //viewControllerDestino.myBool = true
            //viewControllerDestino.myNumero = 100
            //var p = Persona()
            //p.nombre = usernameTextField.text
            //p.edad = 34
            //viewControllerDestino.myPersona = p
            //viewControllerDestino.myString = passTextField.text
            //self.present(viewControllerDestino, animated: true) {
                //print("Los datos se enviaron al VCD")
            //}
        //}
    }
    

}

extension ViewController: DataDelegateBack{
    func didReceiveData(_ data: Persona) {
        //self.currentPersona = data
        self.personas.append(data)
        //dump(self.currentPersona)
    }
}
