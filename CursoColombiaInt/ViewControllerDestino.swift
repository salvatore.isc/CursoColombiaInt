//
//  ViewControllerDestino.swift
//  CursoColombiaInt
//
//  Created by Salvador Lopez on 05/06/23.
//

import UIKit

class ViewControllerDestino: UIViewController {
    
    var color: UIColor?
    
    @IBAction func getBack(_ sender: Any) {
        self.dismiss(animated: true){
            print("Salimos del vcd...")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = color
        
        print("1. ViewDidLoad [VCD]")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("2. ViewWillAppear [VCD]")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("3. ViewDidAppear [VCD]")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("4. ViewWillDissapear [VCD]")
    }

    override func viewDidDisappear(_ animated: Bool) {
        print("5. ViewDidDissapear [VCD]")
    }

}
