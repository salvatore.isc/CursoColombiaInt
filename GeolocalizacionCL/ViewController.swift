//
//  ViewController.swift
//  GeolocalizacionCL
//
//  Created by Salvador Lopez on 09/06/23.
//

import UIKit

class ViewController: UIViewController {
    
    var wScreen = UIScreen.main.bounds.width
    var hScreen = UIScreen.main.bounds.height
    
    var statusLabel: UILabel!
    var locationService = LocationServices()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupStatusLabel()
        
        // Iniciar Location Services
        initLocationServices()
    }
    
    func setupStatusLabel(){
        statusLabel = UILabel(frame: CGRect(x: (wScreen / 2) - 200, y: hScreen/2, width: 400, height: 50))
        statusLabel.font = .systemFont(ofSize: 20)
        statusLabel.textAlignment = .center
        statusLabel.text = "Service Location..."
        statusLabel.numberOfLines = 0
        self.view.addSubview(statusLabel)
    }
    
    func initLocationServices(){
        locationService.delegate = self
        
        //Validar si tengo permisos para track location
        let isEnable = locationService.enable
        if isEnable {
            locationService.requestAuthorization()
        }else{
            locationServicesNeedState()
        }
    }
    
}

extension ViewController: LocationServicesDelegate{
    func authorizationRestricted() {
        locationServicesRestrictedState()
    }
    
    func authorizationUnkwon() {
        locationServicesNeedState()
    }
    
    func promptAuthorizationAction() {
        let alert = UIAlertController(title: "El permiso de ubicacion es necesario para esta app", message: "Por favor acepte los permisos de ubicacion para continuar.", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Ir a configuración", style: .default){
            _ in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .default){
            _ in
            self.locationServicesNeedState()
        }
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        alert.preferredAction = settingsAction
        self.present(alert, animated: true)
    }
    
    func didAuthorized() {
        locationService.start()
    }
}

extension ViewController {
    
    func locationServicesRestrictedState(){
        self.statusLabel.text = "El acceso a los servicios de ubicacion son necesarios para utilizar esta app."
    }
    
    func locationServicesNeedState(){
        self.statusLabel.text = "Esta app esta restringida de los servicios de ubicacion."
    }
    
}

