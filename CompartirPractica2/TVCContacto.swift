//
//  TVCContacto.swift
//  CompartirPractica2
//
//  Created by Salvador Lopez on 07/06/23.
//

import UIKit

class TVCContacto: UITableViewCell {

    @IBOutlet weak var imgContacto: UIImageView!
    @IBOutlet weak var nameContacto: UILabel!
    @IBOutlet weak var paretescoContacto: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
