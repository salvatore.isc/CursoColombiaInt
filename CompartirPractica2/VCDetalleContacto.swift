//
//  VCDetalleContacto.swift
//  CompartirPractica2
//
//  Created by Salvador Lopez on 07/06/23.
//

import UIKit

class VCDetalleContacto: UIViewController {
    
    var currentContact: Contacto?
    
    var textoFelicitar = "¡Felicidades 🥳!"
    var imgFelicitar = UIImage(named: "congrats")
    
    @IBOutlet weak var btnFelicitar: UIButton!
    @IBOutlet weak var nameContact: UITextField!
    @IBOutlet weak var parentescoContact: UITextField!
    @IBOutlet weak var locationContact: UITextField!
    @IBOutlet weak var ageContact: UITextField!
    
    @IBAction func actFelicitar(_ sender: Any) {
        let actController = UIActivityViewController(activityItems: [textoFelicitar,imgFelicitar!], applicationActivities: nil)
        present(actController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Detalle Contacto"
        
        if let contacto = currentContact {
            self.nameContact.text = contacto.nombre
            self.ageContact.text = "\(contacto.edad)"
            self.locationContact.text = contacto.ubicacion.rawValue
            self.parentescoContact.text = contacto.parentesco.rawValue
            self.btnFelicitar.isEnabled = contacto.felicitacion
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Colocar titulo al NavController
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
