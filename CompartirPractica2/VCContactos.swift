//
//  ViewController.swift
//  CompartirPractica2
//
//  Created by Salvador Lopez on 07/06/23.
//

import UIKit

class VCContactos: UIViewController {
    
    var contactos = [
        Contacto(nombre: "Jose", parentesco: .Hermano, mesNac: 6, anioNac: 1989, ubicacion: .Mexico),
        Contacto(nombre: "Luis", parentesco: .Primo, mesNac: 2, anioNac: 1988, ubicacion: .Chile),
        Contacto(nombre: "Ramon", parentesco: .Hermano, mesNac: 6, anioNac: 1976, ubicacion: .Chile),
        Contacto(nombre: "Rogelio", parentesco: .Amigo, mesNac: 4, anioNac: 1984, ubicacion: .Colombia),
        Contacto(nombre: "Josue", parentesco: .Amigo, mesNac: 6, anioNac: 1987, ubicacion: .Colombia)
    ]

    @IBOutlet weak var tableViewContacts: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableViewContacts.dataSource = self
        tableViewContacts.delegate = self
    }

}

extension VCContactos: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vcDetalle = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VCDetalle") as? VCDetalleContacto {
            vcDetalle.currentContact = contactos[indexPath.row]
            //self.present(vcDetalle, animated: true) // Modal
            self.navigationController?.pushViewController(vcDetalle, animated: true)
        }
    }
    
}

extension VCContactos: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contactCell = tableViewContacts.dequeueReusableCell(withIdentifier: "cellContact", for: indexPath) as? TVCContacto
        
        contactCell?.nameContacto.text = contactos[indexPath.row].nombre
        contactCell?.paretescoContacto.text = contactos[indexPath.row].parentesco.rawValue
        
        return contactCell!
    }
    
}
