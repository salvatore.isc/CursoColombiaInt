//
//  Contacto.swift
//  CompartirPractica2
//
//  Created by Salvador Lopez on 07/06/23.
//

import Foundation

enum Parentesco: String{
    case Primo
    case Hermano
    case Tio
    case Amigo
    case Padre
    case Madre
}

enum Ubicacion: String{
    case Mexico
    case Colombia
    case Argentina
    case Chile
    case Brasil
}

class Contacto{
    var nombre: String
    var ubicacion: Ubicacion
    var anioNac: Int
    var mesNac: Int
    var parentesco: Parentesco
    
    init(nombre:String, parentesco:Parentesco, mesNac:Int, anioNac:Int, ubicacion:Ubicacion){
        self.nombre = nombre
        self.parentesco = parentesco
        self.mesNac = mesNac
        self.anioNac = anioNac
        self.ubicacion = ubicacion
    }
    
    var edad:Int {
        let currentYear = 2023
        return currentYear - self.anioNac
    }
    
    var felicitacion: Bool {
        let currentMonth = 6
        return (currentMonth == self.mesNac)
    }
    
}
