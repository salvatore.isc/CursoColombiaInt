//
//  ViewController.swift
//  CoreDataDemo
//
//  Created by Salvador Lopez on 14/06/23.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var users = [User]()
    var idSelected: UUID?
    
    @IBAction func saveUser(_ sender: Any) {
        
        // Instancia AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // Obtener Contexto
        let manageContext = appDelegate.persistentContainer.viewContext
        
        // Nueva Entity
        let userEntity = NSEntityDescription.entity(forEntityName: "User", in: manageContext)
        
        // Crear registro con la entidad User
        let user = NSManagedObject(entity: userEntity!, insertInto: manageContext)
        user.setValue(UUID(), forKey: "id")
        user.setValue(usernameTextField.text, forKey: "username")
        user.setValue(emailTextField.text, forKey: "email")
        user.setValue(passwordTextField.text, forKey: "password")
        
        // Guardar
        do{
            try manageContext.save()
            print("Guardado con exito...")
            loadUsers()
        }catch{
            print("Error: \(error)")
        }
        
        
    }
    
    @IBAction func updateUser(_ sender: Any) {
        // Instancia AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // Obtener Contexto
        let manageContext = appDelegate.persistentContainer.viewContext
        
        // Request + Predicate
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        fetchRequest.predicate = NSPredicate(format: "id == %@", self.idSelected! as CVarArg)
        
        // Fetch
        do {
            let record = try manageContext.fetch(fetchRequest)
            
            if let firstRecord = record.first as? NSManagedObject{
                firstRecord.setValue(self.usernameTextField.text, forKey: "username")
                firstRecord.setValue(self.emailTextField.text, forKey: "email")
                firstRecord.setValue(self.passwordTextField.text, forKey: "password")
                // Save
                do{
                    try manageContext.save()
                    print("Actualizacion con exito...")
                    loadUsers()
                }catch{
                    print("Error: \(error)")
                }
            }
            
        }catch{
            print("Error: \(error)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        loadUsers()
    }
    
    // Fetch Users
    func loadUsers(){
        // Instancia AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        // Obtener Contexto
        let manageContext = appDelegate.persistentContainer.viewContext
        // Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        // Fetch
        do{
            let result = try manageContext.fetch(fetchRequest)
            if result.isEmpty{
                print("No hay registros de [User]")
            }else{
                self.users.removeAll()
                result.forEach { user in
                    if let user = user as? User{
                        self.users.append(user)
                    }
                }
                cleanTextfields()
                tableView.reloadData()
            }
        }catch{
            print("Error: \(error)")
        }
    }
    
    func deleteUser(_ userToDelete:UUID){
        // Instancia AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        // Obtener Contexto
        let manageContext = appDelegate.persistentContainer.viewContext
        // Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        fetchRequest.predicate = NSPredicate(format: "id == %@", userToDelete as CVarArg)
        // Fetch
        do{
            let records = try manageContext.fetch(fetchRequest)
            if let firstRecord = records.first as? NSManagedObject{
                manageContext.delete(firstRecord)
                do{
                    try manageContext.save()
                    print("Eliminado con exito...")
                    self.tableView.reloadData()
                }catch{
                    print("Error: \(error)")
                }
            }
        }catch{
            print("Error: \(error)")
        }
    }
    
    func cleanTextfields(){
        self.usernameTextField.text = ""
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = users[indexPath.row].username
        cell.detailTextLabel?.text = users[indexPath.row].email
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Base de Datos (Users)"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.usernameTextField.text = self.users[indexPath.row].username
        self.emailTextField.text = self.users[indexPath.row].email
        self.passwordTextField.text = self.users[indexPath.row].password
        self.idSelected = self.users[indexPath.row].id
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteUser(self.users[indexPath.row].id!)
        }
    }
    
    //func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        //return true
    //}
    
}
