//
//  DemoUserCoreDataApp.swift
//  DemoUserCoreData
//
//  Created by Salvador Lopez on 15/06/23.
//

import SwiftUI

@main
struct DemoUserCoreDataApp: App {
    let persistenceController = PersistenceController.shared
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
