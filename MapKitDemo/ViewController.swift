//
//  ViewController.swift
//  MapKitDemo
//
//  Created by Salvador Lopez on 13/06/23.
//

import UIKit
import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    var locationManager: CLLocationManager!
    
    /**
     https://www.google.com/maps/place/97751+Chichén-Itzá,+Yucatan/@20.6787816,-88.5710518,17z/data=!3m1!4b1!4m6!3m5!1s0x8f5138b9a098f833:0xf70a67530750d45!8m2!3d20.6791438!4d-88.5683091!16s%2Fg%2F11c5m40nz9?entry=ttu
     */
    
    var latitud: CLLocationDegrees = 20.6787816
    var longitud: CLLocationDegrees = -88.5710518
    
    var latitudDelta: CLLocationDegrees = 0.5 // < +zoom
    var longitudDelta: CLLocationDegrees = 0.5
    
    /**
     https://www.google.com/maps/place/Ik+Kil+Cenote/@20.6640023,-88.551334,17z/data=!4m18!1m8!3m7!1s0x8f5138b9a098f833:0xf70a67530750d45!2s97751+Chichén-Itzá,+Yucatan!3b1!8m2!3d20.6791438!4d-88.5683091!16s%2Fg%2F11c5m40nz9!3m8!1s0x8f513f2904215029:0xfd76468880b9dda9!5m2!4m1!1i2!8m2!3d20.6630185!4d-88.549255!16s%2Fg%2F12xqz3w1h?entry=ttu
     */
    
    var latitudDestino: CLLocationDegrees = 20.6640023
    var longitudDestino: CLLocationDegrees = -88.551334
    
    var latitudDestinoDelta: CLLocationDegrees = 0.2
    var longitudDestinoDelta: CLLocationDegrees = 0.2
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //DispatchQueue.main.asyncAfter(deadline: .now() + 3){
        //initCoordinates()
        //}
        //initAnnotations()
        initPlaceMarks()
        initLocationManager()
        mapView.delegate = self
    }
    
    func initLocationManager(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 10
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        mapView.showsUserLocation = true
    }
    
    func initCoordinates(){
        let span = MKCoordinateSpan(latitudeDelta: latitudDelta, longitudeDelta: longitudDelta)
        let coordinates = CLLocationCoordinate2D(latitude: latitud, longitude: longitud)
        let region = MKCoordinateRegion(center: coordinates, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    func initAnnotations(){
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: latitud, longitude: longitud)
        annotation.title = "Chichén-Itzá"
        annotation.subtitle = "Yucatan"
        mapView.addAnnotation(annotation)
        mapView.selectAnnotation(annotation, animated: true)
        
        let annotationDestino = MKPointAnnotation()
        annotationDestino.coordinate = CLLocationCoordinate2D(latitude: latitudDestino, longitude: longitudDestino)
        annotationDestino.title = "Ik Kil Cenote"
        annotationDestino.subtitle = "Yucatan"
        mapView.addAnnotation(annotationDestino)
        mapView.selectAnnotation(annotationDestino, animated: true)
        
    }
    
    func initPlaceMarks(){
        
        // Coordenadas
        let coordinateOrigin = CLLocationCoordinate2D(latitude: latitud, longitude: longitud)
        let coordinateDestiny = CLLocationCoordinate2D(latitude: latitudDestino, longitude: longitudDestino)
        
        // Placemark / MapItem
        let sourcePlaceMark = MKPlacemark(coordinate: coordinateOrigin)
        let sourceMapItem = MKMapItem(placemark: sourcePlaceMark)
        
        let destinyPlaceMark = MKPlacemark(coordinate: coordinateDestiny)
        let destinyMapItem = MKMapItem(placemark: destinyPlaceMark)
        
        // Request
        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinyMapItem
        directionRequest.transportType = .automobile
        
        // Get Direcctions
        let direccions = MKDirections(request: directionRequest)
        direccions.calculate { responseDirections, errorDirections in
            if let error = errorDirections {
                print("Error al realizar la solicitud para obtener direcciones \(error)...")
            }
            if let response = responseDirections {
                if let ruta = response.routes.first {
                    self.mapView.addOverlay(ruta.polyline, level: .aboveRoads)
                    self.mapView.setRegion(MKCoordinateRegion(ruta.polyline.boundingMapRect), animated: true)
                    print(ruta.name)
                    print(ruta.distance)
                    print(ruta.hasTolls)
                    print(ruta.expectedTravelTime)
                    ruta.steps.forEach { paso in
                        print(paso.instructions)
                    }
                }
                
            }
        }
        
    }


}

extension ViewController: MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = .green
        renderer.lineWidth = 3
        renderer.lineDashPattern = [2,8]
        renderer.alpha = 0.8
        return renderer
    }
    
}

extension ViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error (Location Manager): \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let myLocation: CLLocationCoordinate2D = (manager.location?.coordinate)!
        print("Latitud: \(myLocation.latitude), Longitud: \(myLocation.longitude)")
    }
    
}
