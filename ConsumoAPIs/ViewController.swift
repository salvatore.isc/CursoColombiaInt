//
//  ViewController.swift
//  ConsumoAPIs
//
//  Created by Salvador Lopez on 08/06/23.
//

import UIKit

class ViewController: UIViewController {
    
    let urlServices = "https://netecswift-salvatoreisc.b4a.run"
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func loginAction(_ sender: Any) {
        
        guard self.userNameTextField.text != "" else {
            messageLabel.text = "No se ha ingresado un nombre de usuario."
            return
        }
        guard self.passwordTextField.text != "" else {
            messageLabel.text = "No se ha ingresado un password."
            return
        }
        guard let user = self.userNameTextField.text else {
            messageLabel.text = "No se ha ingresado un nombre de usuario."
            return
        }
        guard let pass = self.passwordTextField.text else {
            messageLabel.text = "No se ha ingresado un password."
            return
        }
        
        var request = URLRequest(url: URL(string: "\(self.urlServices)/login.php?user=\(user)&pass=\(pass)")!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request){
            data, response, error in
            if error != nil {
                print("Error: \(error!)")
            }else{
                print("\t -> RESPONSE: ")
                print(response!)
                print("\t -> DATA: ")
                print(data!)
                print(String(data:data!,encoding: .utf8)!)
                do{
                    let jsonObjResult = try JSONSerialization.jsonObject(with: data!) as! [String:AnyObject]
                    if let status = jsonObjResult["status"]{
                        DispatchQueue.main.async { // Hilo principal APP -> UI
                            self.messageLabel.text = "\(status as! String):".uppercased()
                            if (status as? String) == "ok" {
                                self.performSegue(withIdentifier: "loginSegue", sender: nil)
                            }
                        }
                    }
                    if let message = jsonObjResult["message"]{
                        DispatchQueue.main.async { // Hilo principal APP -> UI
                            self.messageLabel.text! += " \(message as! String)"
                        }
                    }
                }catch{
                    print("Error: \(error)")
                }
            }
        }
        task.resume()
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

