//
//  VCUsers.swift
//  ConsumoAPIs
//
//  Created by Salvador Lopez on 08/06/23.
//

import UIKit

class VCUsers: UIViewController {

    var loadBtn : UIButton!
    let urlService = "https://jsonplaceholder.typicode.com/users"
    var users = [User]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //MARK: loadButton
        loadBtn = UIButton(frame: CGRect(x: 305, y: 90, width: 90, height: 30))
        loadBtn.setTitle("Load", for: .normal)
        loadBtn.backgroundColor = .blue
        loadBtn.tintColor = .black
        loadBtn.addTarget(self, action: #selector(loadUsers), for: .touchUpInside)
        self.view.addSubview(loadBtn)
        
        tableView.dataSource = self
        
    }
    
    @objc func loadUsers(){
        var request = URLRequest(url: URL(string: urlService)!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request){
            data, response, error in
            if error != nil{
                print("Error: \(String(describing: error))")
            }else{
                //print(String(data: data!, encoding: .utf8)!)
                do{
                    let arrUsers = try JSONDecoder().decode([User].self, from: data!)
                    dump(arrUsers)
                    print(type(of: arrUsers))
                    self.users = arrUsers
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }catch{
                    print(error)
                }
            }
        }
        task.resume()
    }

}

extension VCUsers: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "myCell")
        cell.textLabel?.text = self.users[indexPath.row].name
        cell.detailTextLabel?.text = self.users[indexPath.row].username
        return cell
    }

}
