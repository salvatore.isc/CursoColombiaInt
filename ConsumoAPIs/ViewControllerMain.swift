//
//  ViewControllerMain.swift
//  ConsumoAPIs
//
//  Created by Salvador Lopez on 08/06/23.
//

import UIKit
import WebKit

class ViewControllerMain: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = URL(string: "http://www.aprendeinglessila.com/2013/06/this-that-these-those/"){
            let request = URLRequest(url: url)
            webView.load(request)
        }
        
    }

}
