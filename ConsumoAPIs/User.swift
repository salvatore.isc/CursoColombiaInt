//
//  User.swift
//  ConsumoAPIs
//
//  Created by Salvador Lopez on 08/06/23.
//

import Foundation

class User: Decodable{
    var id: Int?
    var name: String?
    var username: String?
    var email: String?
    var address: Address?
    var phone: String?
    var website: String?
    var company: Company?
}

class Address: Decodable{
    var street: String?
    var suite: String?
    var city: String?
    var zipcode: String?
    var geo: Geo?
}

class Geo: Decodable{
    var lat: String?
    var lng: String?
}

class Company: Decodable{
    var name: String?
    var catchPhrase: String?
    var bs: String?
}
