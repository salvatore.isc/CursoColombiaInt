//
//  SwiftUICoreLocationApp.swift
//  SwiftUICoreLocation
//
//  Created by Salvador Lopez on 16/06/23.
//

import SwiftUI

@main
struct SwiftUICoreLocationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
