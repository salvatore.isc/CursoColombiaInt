//
//  ContentView.swift
//  SwiftUICoreLocation
//
//  Created by Salvador Lopez on 16/06/23.
//

import SwiftUI
import CoreLocation
import MapKit

struct ContentView: View {
    
    @StateObject private var locationManager = LocationManager()
    @State private var userLocation: CLLocation?
    @State private var region = MKCoordinateRegion()
    
    var body: some View {
        VStack {
            
            Map(coordinateRegion: $region)
                .frame(width: 300)
            
            if let location = userLocation{
                Text("Latitud: \(location.coordinate.latitude)")
                Text("Longitud: \(location.coordinate.longitude)")
            }else{
                Text("Waiting for location...")
            }
        }
        .onAppear{
            locationManager.requestLocation()
        }
        .onReceive(locationManager.$currentLocation) { location in
            userLocation = location
            updateMapRegion()
        }
    }
    
    func updateMapRegion(){
        guard let location = userLocation else {
            return
        }
        region = MKCoordinateRegion(
            center: location.coordinate,
            span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        )
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
